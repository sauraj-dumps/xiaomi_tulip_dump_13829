## tulip-user 9 PKQ1.180904.001 V12.0.1.0.PEKMIXM release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: tulip
- Brand: xiaomi
- Flavor: tulip-user
- Release Version: 9
- Id: PKQ1.180904.001
- Incremental: V12.0.1.0.PEKMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: xiaomi/tulip/tulip:9/PKQ1.180904.001/V12.0.1.0.PEKMIXM:user/release-keys
- OTA version: 
- Branch: tulip-user-9-PKQ1.180904.001-V12.0.1.0.PEKMIXM-release-keys
- Repo: xiaomi_tulip_dump_13829


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
